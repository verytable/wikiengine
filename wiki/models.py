from django.db import models

# Create your models here.

class Article(models.Model):
    """docstring for Article"""
    title = models.TextField()
    content = models.TextField()