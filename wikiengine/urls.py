from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'wikiengine.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'wiki.views.home', name='home'),
    url(r'^([A-Za-z0-9])+/edit$', 'wiki.views.edit_article', name='edit_article'),
    url(r'^create_article$', 'wiki.views.create_article', name='create_article'),
    url(r'^([A-Za-z0-9]+)$', 'wiki.views.show_article', name='show_article'),
    url(r'^save_article$', 'wiki.views.save_article', name='save_article')
)